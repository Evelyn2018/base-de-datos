### Para desplegar la base de datos por favor tenga en cuenta lo siguiente: ###

1. Asegurarse de tener instalado el motor de base de datos PostgreSQL
2. Descargar y ejecutar los script de la BD que se encuentran en el repositorio, "BdTestEstructura":Contiene las instrucciones para la creación de la base de datos y la tabla. "BdTestData": Contiene las instrucciones para la insercción de datos iniciales.