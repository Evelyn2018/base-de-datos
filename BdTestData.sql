--
-- PostgreSQL database dump
--

-- Dumped from database version 13.4
-- Dumped by pg_dump version 13.4

-- Started on 2021-09-06 19:33:56

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2984 (class 0 OID 16410)
-- Dependencies: 200
-- Data for Name: persona; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.persona (id, nombre, fnacimiento, idpadre, idmadre, documento) OVERRIDING SYSTEM VALUE VALUES (41, 'Juan', '1983-01-04', NULL, NULL, 12355668);
INSERT INTO public.persona (id, nombre, fnacimiento, idpadre, idmadre, documento) OVERRIDING SYSTEM VALUE VALUES (40, 'Angelica', '2014-01-07', 41, NULL, 444555679);
INSERT INTO public.persona (id, nombre, fnacimiento, idpadre, idmadre, documento) OVERRIDING SYSTEM VALUE VALUES (42, 'Pedro', '1946-08-20', NULL, 40, 5326897);
INSERT INTO public.persona (id, nombre, fnacimiento, idpadre, idmadre, documento) OVERRIDING SYSTEM VALUE VALUES (43, 'Maria', '2021-07-14', 41, 40, 789563);


--
-- TOC entry 2991 (class 0 OID 0)
-- Dependencies: 201
-- Name: persona_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.persona_id_seq', 43, true);


-- Completed on 2021-09-06 19:33:56

--
-- PostgreSQL database dump complete
--

